#!/usr/bin/env python
import sys
import unittest


# Run all tests
def main(pattern='test_*.py', *args):
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('tests', pattern=pattern)
    runner = unittest.TextTestRunner()
    results = runner.run(test_suite)
    return results.wasSuccessful()


if __name__ == '__main__':
    sys.exit(0 if main(*sys.argv[1:]) else 1)
