include { p1  } from '../modules/p1'

workflow wf1 {
    take:
    input_wf1

    main:
    output_wf1 = p1(input_wf1)

    emit:
    //val = output_wf1
    output_wf1
}