/*
  This is a sample Nextflow script to test the comment removal function. Just writing this to add an "'" like if i wrote "it's"
*/

/*
https://www.nextflow.io/
*/

// Define a process that echoes a message
process echoMessage {

    // Input parameters
    input:
    // This is an inline comment
    val message

    // Output
    output:
    // Single-line comment
    path "/*.txt"

    // Script section
    script:
    """
    echo "Message: $message" > output.txt
    #https://www.nextflow.io/
    """

    
}