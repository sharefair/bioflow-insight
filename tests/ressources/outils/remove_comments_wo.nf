




process echoMessage {

    
    input:
    
    val message

    
    output:
    
    path "/*.txt"

    
    script:
    """
    echo "Message: $message" > output.txt
    #https://www.nextflow.io/
    """

    
}


