//Taken from https://github.com/nf-core/mhcquant/blob/b80a5a4fbf1ff4d409885d08ab09f6ceeb7fe4c9/modules/local/openms_falsediscoveryrate.nf
//+ modified

process OPENMS_FALSEDISCOVERYRATE  {
    tag "$meta.id"
    label 'process_single'

    conda "bioconda::openms=3.0.0"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/openms:3.0.0--h8964181_1' :
        'biocontainers/openms:3.0.0--h8964181_1' }"

    input:
        tuple val(meta), path(idxml)

    output:
        tuple val(meta), path("*.idXML"), emit: idxml
        path "versions.yml"             , emit: versions

    when:
        task.ext.when == null || task.ext.when

    script:
        def prefix           = task.ext.prefix ?: "${idxml.baseName}_fdr"

        """
        FalseDiscoveryRate -in $idxml \\
            -protein 'false' \\
            -out ${prefix}.idXML \\
            -threads $task.cpus

        cat <<-END_VERSIONS > versions.yml
        "${task.process}":
            openms: \$(echo \$(FileInfo --help 2>&1) | sed 's/^.*Version: //; s/-.*\$//' | sed 's/ -*//; s/ .*\$//')
        END_VERSIONS
        """
}

/*

//wtsi-hgi/nf_cellbender/modules/core.nf

input:
        val(outdir_prev)
        tuple(
            val(experiment_id),
            path(file_10x_barcodes),
            path(file_10x_features),
            path(file_10x_matrix),
            val(ncells_expected),
            val(ndroplets_include_cellbender)
        )
        val(estimate_params_umis)

    output:
        val(outdir, emit: outdir)
        tuple(
            val(experiment_id),
            path(file_10x_barcodes),
            path(file_10x_features),
            path(file_10x_matrix),
            path("${outfile}-expected_cells.txt"),
            path("${outfile}-total_droplets_included.txt"),
            emit: cb_input
        )
        path(
            "${outfile}-expected_cells.txt",
            emit: expected_cells
        )
        path(
            "${outfile}-total_droplets_included.txt",
            emit: total_droplets_include
        )
        path("${outfile}-cell_estimate_cutoff.tsv.gz")
        path("${outfile}-total_droplets_cutoff.tsv.gz")
        path("plots/*.png") optional true
        path("plots/*.pdf") optional true

*/

