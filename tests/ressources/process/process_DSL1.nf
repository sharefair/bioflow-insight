//Taken from https://github.com/maxemil/ALE-pipeline/blob/c8f17b11dd3496420cfcb4a5c29564d2257eabf4/main.nf
//+ modified

process cleanSpeciesTree {
  input:
  file species_tree
  file 'map_species.txt' from species_map.first()

  output:
  file "${species_tree.baseName}_clean.tree" into clean_species_tree
  file "${species_tree.baseName}_root.tree" into rooted_species_tree

  publishDir params.output_trees, mode: 'copy'
  tag {"${species_tree.simpleName}"}

  script:
  template 'cleanSpeciesTree.py'
}