//GiantSpaceRobot/tsRNAsearch

DESEQ2(COUNTS_TO_COLLAPSED_COUNTS.out.collapsed_count.collect(), "$layoutfile", PREPARE_NCRNA_GTF.out.ncRNA_gtf)
DATA_TRANSFORMATIONS("$layoutfile", \
    GENERATE_TRNA_DEPTH_FILES.out.depth_files.collect(), \
    GENERATE_NCRNA_DEPTH_FILES.out.depth_files.collect(), \
    GENERATE_MULTIMAPPER_TRNA_DEPTH_FILES.out.depth_files.collect(), \
    SUM_COUNTS.out.sum_counts)
DISTRIBUTION_SCORE(DATA_TRANSFORMATIONS.out.ncrna_stddev, DATA_TRANSFORMATIONS.out.trna_stddev, PREPARE_NCRNA_GTF.out.ncRNA_gtf)
SLOPE_SCORE(DATA_TRANSFORMATIONS.out.depth_means, "$layoutfile", PREPARE_NCRNA_GTF.out.ncRNA_gtf)


//Case where call.into{ch1, ch2}